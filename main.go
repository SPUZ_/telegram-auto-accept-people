package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

var (
	token  = ""
	offset = 0
	delay  = 1
)

type OnePersonToAccept struct {
	ChatID int64
	UserID int64
	Data   TelegramFrom
}

func getUpdates() ([]OnePersonToAccept, error) {
	// Getting updates from Telegram
	url := fmt.Sprintf("https://api.telegram.org/bot%s/getUpdates?offset=%d", token, offset)
	response, _ := http.Get(url)

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Printf("Error at closing http getUpdates: %v", err)
		}
	}(response.Body)

	var result UpdateResponse
	err := json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		log.Printf("Error at unmarshal getUpdates: %v", err)
		return []OnePersonToAccept{}, err
	}

	// Getting only chat_join_request
	var example ResponseChatJoinRequest
	var toAccept []OnePersonToAccept

	for _, data := range result.Result {
		// if data in response is equal to clean ResponseChatJoinRequest struct it's something else
		if data.ChatJoinRequest != example {
			if data.UpdateId != offset {
				toAccept = append(toAccept, OnePersonToAccept{
					ChatID: data.ChatJoinRequest.Chat.Id,
					UserID: data.ChatJoinRequest.From.Id,
					Data:   data.ChatJoinRequest.From,
				})
				offset = data.UpdateId
			}
		}
	}

	if len(toAccept) > 0 {
		log.Println(toAccept)
	}

	return toAccept, nil
}

func acceptPerson(toAccept OnePersonToAccept) {
	// https://api.telegram.org/bot5781055914:AAH6aOp5X7gxbCBd6lJ4OokUaPJI1cJkB9M/
	url := fmt.Sprintf("https://api.telegram.org/bot%s/approveChatJoinRequest?user_id=%d&chat_id=%d", token, toAccept.UserID, toAccept.ChatID)
	response, _ := http.Get(url)

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Printf("Error at closing http getUpdates: %v", err)
		}
	}(response.Body)
}

func main() {
	token = os.Getenv("TOKEN")
	delayFromEnv := os.Getenv("DELAY")
	if delayFromEnv != "" {
		var err error
		delay, err = strconv.Atoi(delayFromEnv)
		if err != nil {
			log.Println("I need INT DELAY")
			time.Sleep(60 * time.Second)
		}
	}

	if token == "" {
		fmt.Println("I need Telegram bot token")
		os.Exit(0)
	}
	log.Println("STARTED")
	for true {
		updates, err := getUpdates()
		if err != nil {
			log.Println("ERROR AT getUpdates")
			time.Sleep(60 * time.Second)
		}

		for _, update := range updates {
			acceptPerson(update)
		}
		time.Sleep(time.Duration(delay) * time.Second)
	}
}

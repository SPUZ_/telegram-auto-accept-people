package main

type UpdateResponse struct {
	Ok     bool             `json:"ok"`
	Result []ResultInUpdate `json:"result"`
}

type ResultInUpdate struct {
	UpdateId        int                     `json:"update_id"`
	ChatJoinRequest ResponseChatJoinRequest `json:"chat_join_request"`
}

type ResponseChatJoinRequest struct {
	Chat       TelegramChat       `json:"chat"`
	From       TelegramFrom       `json:"from"`
	Date       int                `json:"date"`
	InviteLink TelegramInviteLink `json:"invite_link"`
}

type TelegramFrom struct {
	Id           int64  `json:"id"`
	IsBot        bool   `json:"is_bot"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	LanguageCode string `json:"language_code"`
	Username     string `json:"username,omitempty"`
}

type TelegramChat struct {
	Id    int64  `json:"id"`
	Title string `json:"title"`
	Type  string `json:"type"`
}

type TelegramCreator struct {
	Id           int64  `json:"id"`
	IsBot        bool   `json:"is_bot"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Username     string `json:"username"`
	LanguageCode string `json:"language_code"`
	IsPremium    bool   `json:"is_premium"`
}

type TelegramInviteLink struct {
	InviteLink              string          `json:"invite_link"`
	Name                    string          `json:"name"`
	Creator                 TelegramCreator `json:"creator"`
	PendingJoinRequestCount int             `json:"pending_join_request_count"`
	CreatesJoinRequest      bool            `json:"creates_join_request"`
	IsPrimary               bool            `json:"is_primary"`
	IsRevoked               bool            `json:"is_revoked"`
}
